/**
 * File: options-l10n.js
 *
 * Copyright (c) 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const internationalizeOptionsPageStrings = () => {
    document.getElementById('options-title').textContent = browser.i18n.getMessage('optionsPageTitle');
    document.getElementById('options-page-action').textContent = browser.i18n.getMessage('optionsShowPageActionButton');
    document.getElementById('options-context-menu').textContent = browser.i18n.getMessage('optionsShowCloseOverlayContextMenu');
    document.getElementById('save-options').textContent = browser.i18n.getMessage('optionsSaveOptionsButton');
};

internationalizeOptionsPageStrings();
